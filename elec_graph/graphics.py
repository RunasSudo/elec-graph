from .config import *

import numpy
import PIL.Image, PIL.ImageDraw
import pygame

import math

# Load fonts
fonp_ssp = pygame.font.match_font('Source Sans Pro')
fonp_ssp = fonp_ssp[:fonp_ssp.rindex('-')] + '-Regular' + fonp_ssp[fonp_ssp.rindex('.'):]
font_ssp18 = pygame.font.Font(fonp_ssp, 24)
font_ssp18_sb = pygame.font.Font(fonp_ssp.replace('-Regular', '-Semibold'), 24)
font_ssp20 = pygame.font.Font(fonp_ssp, 26)
font_ssp30 = pygame.font.Font(fonp_ssp, 40)
font_ssp30_sb = pygame.font.Font(fonp_ssp.replace('-Regular', '-Semibold'), 40)
font_ssp38_sb = pygame.font.Font(fonp_ssp.replace('-Regular', '-Semibold'), 50)
font_ssp72_sb = pygame.font.Font(fonp_ssp.replace('-Regular', '-Semibold'), 96)

nblack = (26, 26, 26)

class SurfaceXY:
	def __init__(self, surface, rect=None):
		self.surface = surface
		self.rect = rect if rect else surface.get_rect()
	
	def draw(self, surface):
		surface.blit(self.surface, self.rect)

# Wrap some attributes
def wrap_rect_attr(attr):
	def getter(self):
		return getattr(self.rect, attr)
	def setter(self, val):
		return setattr(self.rect, attr, val)
	return property(getter, setter)
for attr in ['x', 'y', 'top', 'left', 'bottom', 'right', 'topleft', 'bottomleft', 'topright', 'bottomright', 'midtop', 'midleft', 'midbottom', 'midright', 'center', 'centerx', 'centery', 'width', 'height']:
	setattr(SurfaceXY, attr, wrap_rect_attr(attr))

# Specific graphics

class BackgroundGraphic:
	def __init__(self):
		surf_bg = pygame.image.load('img/parlhouse.jpg')
		surf_bg = pygame.transform.smoothscale(surf_bg, (WIDTH, HEIGHT))
		self.surf_bg = SurfaceXY(surf_bg)
		
		surf_bg2 = pygame.image.load('img/parlhouse_centblur.jpg')
		#surf_bg2 = pygame.transform.smoothscale(surf_bg2, (WIDTH - SP12, HEIGHT - SP12))
		self.surf_bg2 = SurfaceXY(surf_bg2)
		self.surf_bg2.center = self.surf_bg.center
		
		self.surf_bg3 = pygame.Surface((WIDTH - 2*SP6, HEIGHT - 2*SP6)) # Not SurfaceXY
		self.surf_bg3.set_alpha(170)
		self.surf_bg3.fill((255, 255, 255))
	
	def draw(self, surface):
		self.surf_bg.draw(surface)
		self.surf_bg2.draw(surface)
		surface.blit(self.surf_bg3, self.surf_bg2.rect) # Reuse rect

class CornerGraphic:
	def __init__(self):
		self.surf_corner1 = SurfaceXY(font_ssp18.render(' Votes', True, (255, 255, 255)))
		self.surf_corner1.right = WIDTH - SP6
		self.surf_corner1.centery = HEIGHT - SP6/2
		
		self.surf_corner2 = SurfaceXY(font_ssp18_sb.render('AustraliaSim', True, (255, 255, 255)))
		self.surf_corner2.right = self.surf_corner1.left
		self.surf_corner2.y = self.surf_corner1.y
		
		self.surf_aus = SurfaceXY(pygame.image.load('img/australia.png'))
		self.surf_aus.right = self.surf_corner2.left - SP1
		self.surf_aus.centery = self.surf_corner1.centery
	
	def draw(self, surface):
		self.surf_corner1.draw(surface)
		self.surf_corner2.draw(surface)
		self.surf_aus.draw(surface)

class TitleGraphic:
	def __init__(self, title, suppl):
		self.surf_title = SurfaceXY(font_ssp38_sb.render(title, True, (255, 255, 255)))
		self.surf_title.x = SP10
		self.surf_title.centery = SP8 + SP10/2
		
		self.surf_suppl = SurfaceXY(font_ssp20.render(suppl, True, (255, 255, 255)))
		self.surf_suppl.right = WIDTH - SP10
		self.surf_suppl.centery = SP8 + SP10/2
	
	def draw(self, surface):
		surface.fill(nblack, pygame.Rect((SP8, SP8, WIDTH - 2*SP8, SP10)))
		self.surf_title.draw(surface)
		self.surf_suppl.draw(surface)

class MPBoxGraphic:
	def __init__(self, colour):
		self.rect_box = pygame.Rect((0, 0, SP6, 3.5*SP1))
		self.surf_text = SurfaceXY(font_ssp18.render('MP', True, colour))
	
	def draw(self, surface):
		surface.fill((255, 255, 255), self.rect_box)
		self.surf_text.draw(surface)

class FPVGraphic:
	def __init__(self, n, party, is_mp, name, colour, num1, num2, width2):
		self.colour = colour
		self.width2 = width2
		
		self.surf_party = SurfaceXY(font_ssp30.render(party, True, (255, 255, 255)))
		self.surf_party.x = SP10
		
		self.mpbox = None
		if is_mp:
			self.mpbox = MPBoxGraphic(colour)
			self.mpbox.rect_box.centerx = SP10 + 17*SP1
			self.mpbox.surf_text.centerx = SP10 + 17*SP1
		
		self.surf_name = SurfaceXY(font_ssp30_sb.render(name, True, (255, 255, 255)))
		self.surf_name.x = SP10 + 22*SP1
		
		self.set_n(n)
		
		self.surf_num1 = SurfaceXY(font_ssp30_sb.render(num1, True, (255, 255, 255)))
		self.surf_num1.right = WIDTH - SP10
		
		self.surf_num2 = None
		if num2:
			self.surf_num2 = SurfaceXY(font_ssp30_sb.render(num2, True, (255, 255, 255)))
			self.surf_num2.right = WIDTH - SP10
		
		self.set_progress(0.0)
		
		self.rect_area = pygame.Rect((SP8, self.y, (WIDTH - 2*SP8), SP8))
	
	def set_n(self, n):
		self.y = SP8 + SP10 + SP1 + n*9*SP1
		self.surf_party.centery = self.y + SP4
		self.surf_name.centery = self.surf_party.centery
		if self.mpbox:
			self.mpbox.rect_box.centery = self.surf_party.centery
			self.mpbox.surf_text.centery = self.surf_party.centery
	
	def set_progress(self, progress):
		self.progress = progress
		self.surf_num1.centery = self.surf_party.centery - SP8 * progress
		if self.surf_num2:
			self.surf_num2.centery = self.surf_party.centery + SP8 * (1.0 - progress)
	
	def draw(self, surface, blank=True):
		surface.set_clip(self.rect_area) # Restrict to area
		
		if blank:
			surface.fill(nblack)
		surface.fill(self.colour, pygame.Rect((SP8, self.y, (WIDTH - 2*SP8) * (1.0 - (1.0 - self.width2) * self.progress), SP8)))
		self.surf_party.draw(surface)
		self.surf_name.draw(surface)
		
		self.surf_num1.draw(surface)
		if self.surf_num2:
			self.surf_num2.draw(surface)
		
		if self.mpbox:
			self.mpbox.draw(surface)
		
		surface.set_clip(None)

def lighten(colour, factor):
	def lp(x):
		return 255 - ((255 - x) * (1 - factor))
	return (lp(colour[0]), lp(colour[1]), lp(colour[2]))

class FPVGraphic2:
	def __init__(self, n, colour, num1, num2, width1, width2, grph_fpv):
		self.colour = lighten(colour, 0.4)
		self.width1 = width1
		self.width2 = width2
		self.grph_fpv = grph_fpv
		
		self.set_n(n)
		
		self.surf_num1 = SurfaceXY(font_ssp30_sb.render(num1, True, (255, 255, 255)))
		self.surf_num1.right = WIDTH - SP10
		
		self.surf_num2 = None
		if num2:
			self.surf_num2 = SurfaceXY(font_ssp30_sb.render(num2, True, (255, 255, 255)))
			self.surf_num2.right = WIDTH - SP10
		
		self.set_progress(0.0)
	
	def set_n(self, n):
		self.y = SP8 + SP10 + SP1 + n*9*SP1
	
	def set_progress(self, progress):
		self.progress = progress
		self.surf_num1.centery = self.grph_fpv.surf_party.centery - SP8 * progress
		if self.surf_num2:
			self.surf_num2.centery = self.grph_fpv.surf_party.centery + SP8 * (1.0 - progress)
	
	def draw(self, surface):
		surface.set_clip(self.grph_fpv.rect_area) # Restrict to area
		
		surface.fill(nblack)
		surface.fill(self.colour, pygame.Rect((SP8, self.y, (WIDTH - 2*SP8) * ((self.width2 - self.width1) * self.progress + self.width1), SP8)))
		
		self.surf_num1.draw(surface)
		if self.surf_num2:
			self.surf_num2.draw(surface)
		
		surface.set_clip(None)

# oof, pygame's arc drawing function is borked
def draw_arc(colour, rect, rect_dst, angle1, angle2, width):
	image = PIL.Image.new('RGBA', (rect_dst.width, rect_dst.height))
	idraw = PIL.ImageDraw.Draw(image)
	idraw.arc([rect.left, rect.top, rect.right, rect.bottom], angle1, angle2, colour, width)
	
	surf = pygame.image.fromstring(image.tobytes(), image.size, image.mode) # jesus fuck
	
	# Fill in the gaps
	# pygame and PIL both give stupid gaps, but PIL's is nicer
	# This algorithm is OK because the arc should never go into the bottom half of the circle
	pixels = pygame.surfarray.pixels2d(surf)
	for row in pixels:
		indexes = numpy.nonzero(row)[0]
		if len(indexes):
			imin = numpy.amin(indexes)
			imax = numpy.amax(indexes)
			row[imin:imax] = row[imin]
	
	return surf

class TCPGraphic:
	def __init__(self, margin, cand1, cand2):
		self.margin = margin
		self.cand1 = cand1
		self.cand2 = cand2
		
		# The location of the destination arc surface
		self.rect_dstarc = pygame.Rect((0, 0, 28*SP1*2 + 7*SP1, 28*SP1*2 + 7*SP1))
		self.rect_dstarc.centerx = WIDTH / 2
		self.rect_dstarc.centery = HEIGHT - 17*SP1
		
		# The specifications of the arc within rect_dstarc
		self.rect_arc = pygame.Rect((0, 0, 28*SP1*2, 28*SP1*2))
		self.rect_arc.centerx = self.rect_dstarc.width / 2
		self.rect_arc.centery = self.rect_dstarc.height / 2
		
		self.surf_margin = SurfaceXY(font_ssp72_sb.render('{:.1f}%'.format(abs(self.margin) * 100), True, nblack))
		self.surf_margin.centerx = WIDTH / 2
		self.surf_margin.bottom = self.rect_dstarc.centery + SP1
		
		self.rect_margin_area = pygame.Rect((self.surf_margin.x, self.surf_margin.y, self.surf_margin.width, self.surf_margin.height))
		self.surf_margin.y += self.surf_margin.height
		
		self.surf_label = SurfaceXY(font_ssp30.render('MARGIN', True, nblack))
		self.surf_label.centerx = WIDTH / 2
		self.surf_label.top = self.rect_dstarc.centery - SP1
		
		self.set_progress(0.0)
	
	def set_progress(self, progress):
		self.progress = progress
		
		scaled = (abs(self.margin)/1.01)**(1/3)
		if self.margin < 0:
			scaled = -scaled
		deg_disp = scaled * 90 * progress
		
		self.surf_arc2 = draw_arc(self.cand2.colour, self.rect_arc, self.rect_dstarc, -90 + deg_disp, 0, 7*SP1)
		self.surf_arc1 = draw_arc(self.cand1.colour, self.rect_arc, self.rect_dstarc, -180, -90 + deg_disp + 1, 7*SP1) # 1 degree overlap
		
		self.surf_margin.y = self.rect_margin_area.y + (1.0 - progress) * self.surf_margin.height
		
		if self.cand1.surf_tick:
			self.cand1.surf_tick.y = self.cand1.rect_tick_area.y - (1.0 - progress) * self.cand1.surf_tick.height
		if self.cand2.surf_tick:
			self.cand2.surf_tick.y = self.cand2.rect_tick_area.y - (1.0 - progress) * self.cand2.surf_tick.height
	
	def draw(self, surface):
		surface.blit(self.surf_arc1, self.rect_dstarc)
		surface.blit(self.surf_arc2, self.rect_dstarc)
		
		surface.set_clip(self.rect_margin_area) # Restrict to area
		self.surf_margin.draw(surface)
		surface.set_clip(None)
		self.surf_label.draw(surface)
		
		self.cand1.draw(surface)
		self.cand2.draw(surface)

class TCPCandidate:
	def __init__(self, name, colour, is_mp, is_left, is_winner):
		self.colour = colour
		
		self.rect_box = pygame.Rect((0, 0, 40*SP1, 25*SP1))
		self.surf_name = SurfaceXY(font_ssp18.render(name, True, (255, 255, 255)))
		
		self.rect_box.top = SP8 + SP10 + 5*SP1
		self.surf_name.bottom = self.rect_box.bottom - SP1
		
		self.mpbox = None
		if is_mp:
			self.mpbox = MPBoxGraphic(colour)
			self.mpbox.rect_box.bottom = self.rect_box.bottom - SP1
			self.mpbox.surf_text.centery = self.mpbox.rect_box.centery
		
		self.surf_tick = None
		if is_winner:
			self.surf_tick = SurfaceXY(pygame.image.load('img/tick.png'))
			self.surf_tick.top = self.rect_box.top + SP1
		
		if is_left:
			self.rect_box.left = SP10
			self.surf_name.left = self.rect_box.left + 2*SP1
			if is_mp:
				self.mpbox.rect_box.right = self.rect_box.right - SP1
				self.mpbox.surf_text.centerx = self.mpbox.rect_box.centerx
			if is_winner:
				self.surf_tick.right = self.rect_box.right - SP1
		else:
			self.rect_box.right = WIDTH - SP10
			self.surf_name.right = self.rect_box.right - 2*SP1
			if is_mp:
				self.mpbox.rect_box.left = self.rect_box.left + SP1
				self.mpbox.surf_text.centerx = self.mpbox.rect_box.centerx
			if is_winner:
				self.surf_tick.left = self.rect_box.left + SP1
		
		self.rect_tick_area = None
		if is_winner:
			self.rect_tick_area = pygame.Rect((self.surf_tick.x, self.surf_tick.y, self.surf_tick.width, self.surf_tick.height))
			self.surf_tick.y -= self.surf_tick.height
		
		surf_img = pygame.image.load('avatar/' + name + '.png')
		surf_img = pygame.transform.smoothscale(surf_img, (16*SP1, 16*SP1))
		self.surf_img = SurfaceXY(surf_img)
		self.surf_img.centerx = self.rect_box.centerx
		self.surf_img.centery = self.rect_box.centery - 2*SP1
	
	def draw(self, surface):
		surface.fill(self.colour, self.rect_box)
		self.surf_name.draw(surface)
		
		self.surf_img.draw(surface)
		
		if self.mpbox:
			self.mpbox.draw(surface)
		
		if self.surf_tick:
			surface.set_clip(self.rect_tick_area) # Restrict to area
			self.surf_tick.draw(surface)
			surface.set_clip(None)
