#!/usr/bin/env python3

import pygame
pygame.init()

from .config import *
from .graphics import *

import os.path
import struct
import sys
import yaml

# Load data
with open(sys.argv[1], 'r') as f:
	data = yaml.load(f)

parties = {}
for party, colour in data['parties'].items():
	parties[party] = struct.unpack('BBB', bytes.fromhex(colour[1:]))

# Check avatars
for seat in data['house']:
	for cand in seat['cands']:
		if len(cand) > 5:
			if not os.path.exists('avatar/' + cand[0] + '.png'):
				raise Exception('No avatar for ' + cand[0])

screen = pygame.display.set_mode((WIDTH, HEIGHT))

def draw_seat(num):
	seat = data['house'][num]
	seat['cands'].sort(key=lambda cand: cand[3], reverse=True)
	real_votes = sum(cand[3] for cand in seat['cands'])
	total_votes = sum(cand[4] for cand in seat['cands'])
	tcp = [cand for cand in seat['cands'] if len(cand) > 5]
	
	# Preload surfaces
	grph_background = BackgroundGraphic()
	grph_corner = CornerGraphic()
	grph_title = TitleGraphic(seat['seat'], '{:.1f}% turnout'.format(real_votes / seat['voters'] * 100))
	
	grphs_fpv = []
	for i, candidate in enumerate(seat['cands']):
		grphs_fpv.append(FPVGraphic(i, candidate[1], candidate[2], candidate[0], parties[candidate[1]], '{:,}'.format(candidate[3]).replace(',', '\u2009'), '{:.1f}%'.format(candidate[3] / total_votes * 100), candidate[3] / total_votes))
	
	grphs_fpv2 = []
	for i, candidate in enumerate(seat['cands']):
		grphs_fpv2.append(FPVGraphic2(i, parties[candidate[1]], '{:.1f}%'.format(candidate[3] / total_votes * 100), '{:.1f}%'.format(candidate[4] / total_votes * 100), candidate[3] / total_votes, candidate[4] / total_votes, grphs_fpv[i]))
	
	grph_tcp = TCPGraphic(
		(tcp[0][5] - tcp[1][5]) / (tcp[0][5] + tcp[1][5]),
		TCPCandidate(tcp[0][0], parties[tcp[0][1]], tcp[0][2], True, tcp[0][6]),
		TCPCandidate(tcp[1][0], parties[tcp[1][1]], tcp[1][2], False, tcp[1][6])
	)
	
	clock = pygame.time.Clock()
	
	def process_events():
		ret = None
		
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				sys.exit()
			
			if event.type == pygame.KEYUP:
				if event.key == pygame.K_SPACE:
					ret = pygame.K_SPACE
		
		return ret
	
	def wait_advance_space():
		key = process_events()
		if key == pygame.K_SPACE:
			return False
		return True
	
	def wait_advance_timer():
		process_events()
		pygame.time.wait(3000)
	
	wait_advance = wait_advance_timer
	
	def draw_base():
		pygame.display.set_caption('FPS: {:.1f}'.format(clock.get_fps()))
		
		# Draw background images
		grph_background.draw(screen)
		
		# Draw corner text
		grph_corner.draw(screen)
	
	def draw_0(prop):
		draw_base()
		
		# Draw 1 to surface
		surf2 = pygame.Surface((WIDTH, HEIGHT), flags=pygame.SRCALPHA)
		grph_title.draw(surf2)
		for grph_fpv in grphs_fpv:
			grph_fpv.draw(surf2)
		
		# Interpolate
		surf_alpha = pygame.Surface((WIDTH, HEIGHT), flags=pygame.SRCALPHA)
		surf_alpha.fill((255, 255, 255, 255 * prop))
		surf2.blit(surf_alpha, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)
		
		# Display
		screen.blit(surf2, (0, HEIGHT * (1.0 - prop)))
		
		pygame.display.flip()
		clock.tick(1000 // FPS)
	
	def draw_1():
		draw_base()
		
		# Draw title
		grph_title.draw(screen)
		
		# Draw candidates
		for grph_fpv in grphs_fpv:
			grph_fpv.draw(screen)
		
		pygame.display.flip()
		clock.tick(1000 // FPS)
	
	def draw_1aa():
		draw_base()
		
		# Draw title
		grph_title.draw(screen)
		
		# Draw candidates
		for grph_fpv2 in grphs_fpv2:
			grph_fpv2.draw(screen)
		for grph_fpv in grphs_fpv:
			grph_fpv.draw(screen, False)
		
		pygame.display.flip()
		clock.tick(1000 // FPS)
	
	def draw_1a(prop):
		draw_base()
		
		# Draw 1aa to surface
		surf1 = pygame.Surface((WIDTH, HEIGHT), flags=pygame.SRCALPHA)
		grph_title.draw(surf1)
		for grph_fpv2 in grphs_fpv2:
			grph_fpv2.draw(surf1)
		for grph_fpv in grphs_fpv:
			grph_fpv.draw(surf1, False)
		
		# Draw 2 to surface
		surf2 = pygame.Surface((WIDTH, HEIGHT), flags=pygame.SRCALPHA)
		grph_title.draw(surf2)
		grph_tcp.draw(surf2)
		
		# Interpolate
		surf_alpha = pygame.Surface((WIDTH, HEIGHT), flags=pygame.SRCALPHA)
		surf_alpha.fill((255, 255, 255, 255 * (1.0 - prop)))
		surf1.blit(surf_alpha, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)
		surf_alpha.fill((255, 255, 255, 255 * prop))
		surf2.blit(surf_alpha, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)
		
		# Display
		screen.blit(surf1, (0, -HEIGHT * prop))
		screen.blit(surf2, (0, HEIGHT * (1.0 - prop)))
		
		pygame.display.flip()
		clock.tick(1000 // FPS)
	
	def draw_2():
		draw_base()
		
		# Draw title
		grph_title.draw(screen)
		
		# Draw wheel
		grph_tcp.draw(screen)
		
		pygame.display.flip()
		clock.tick(1000 // FPS)
	
	def draw_3(prop):
		draw_base()
		
		# Draw 2 to surface
		surf1 = pygame.Surface((WIDTH, HEIGHT), flags=pygame.SRCALPHA)
		grph_title.draw(surf1)
		grph_tcp.draw(surf1)
		
		# Interpolate
		surf_alpha = pygame.Surface((WIDTH, HEIGHT), flags=pygame.SRCALPHA)
		surf_alpha.fill((255, 255, 255, 255 * (1.0 - prop)))
		surf1.blit(surf_alpha, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)
		
		# Display
		screen.blit(surf1, (0, -HEIGHT * prop))
		
		pygame.display.flip()
		clock.tick(1000 // FPS)
	
	def interpolate(secs):
		maxit = int(secs * FPS)
		for i in range(maxit + 1):
			progress = i / maxit
			yield 1.0 - (1.0 - progress)**2
	
	# Blank
	if num == 0:
		draw_0(0)
		while wait_advance():
			draw_0(0)
	
	# Animate entry
	for prop in interpolate(0.3):
		draw_0(prop)
	
	# FPV screen
	while wait_advance():
		draw_1()
	
	# Animate
	for prop in interpolate(0.6):
		for grph_fpv in grphs_fpv:
			grph_fpv.set_progress(prop)
		draw_1()
	
	# FPV screen (%)
	#while wait_advance():
	#	draw_1()
	pygame.time.wait(500)
	
	# Animate
	for grph_fpv in grphs_fpv:
		grph_fpv.surf_num2 = None
	for prop in interpolate(0.6):
		for grph_fpv2 in grphs_fpv2:
			grph_fpv2.set_progress(prop)
		draw_1aa()
	
	# FPV screen + modifiers
	while wait_advance():
		draw_1aa()
	
	# Animate screen transition
	for prop in interpolate(0.6):
		draw_1a(prop)
	
	# 2CP screen
	#while wait_advance():
	#	draw_2()
	
	# Animate
	for prop in interpolate(0.6):
		grph_tcp.set_progress(prop)
		draw_2()
	
	# 2CP screen (result)
	while wait_advance():
		draw_2()
	
	# Animate exit
	for prop in interpolate(0.3):
		draw_3(prop)

for i in range(len(data['house'])):
	draw_seat(i)
